import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import numpy as np
import random, copy


class eight_queens_problem(object):
    @property
    def population(self):
        return self._population
    
    @property
    def population_size(self):
        return self._population_size

    @property
    def evaluations(self):
        return self._evaluations

    @property
    def n_generation(self):
        return self._n_generation
    
    @property
    def permutation(self):
        return self._permutation
    
    @property
    def max_evaluations(self):
        return self._max_evaluations
    
    @property
    def random_selections(self):
        return self._random_selections

    def __init__(self):
        self._n_generation = 0
        self._evaluations = 0
        self._max_evaluations = 30000
        self._population = []
        self._population_size = 100
        self._permutation = []
        self._random_selections = 5
        for i in range(self._population_size):
            self._population.append(random.sample(range(1,9),8))


    def generation(self):
        candid_parents = []
        candid_fitness = []
        for i in range(self.random_selections):
            candid_parents.append(self._population[random.randint(0, len(self._population)-1)])
            self._evaluations += 1
            candid_fitness.append(self.collisions(candid_parents[i]))
            sorted_fitness = copy.deepcopy(candid_fitness)
        sorted_fitness.sort(reverse=True)
        parent1 = candid_parents[candid_fitness.index(sorted_fitness[0])]
        parent2 = candid_parents[candid_fitness.index(sorted_fitness[1]) - 1]
        # mating include recombination & mutation
        child1, child2 = self.recombination(parent1, parent2)
        child1 = self.mutation(child1)
        child2 = self.mutation(child2)
        # sort childs according to thier fitness. child1 is better one
        if self.collisions(child1) > self.collisions(child2):
            child1, child2 = child2, child1
        # survivor selection
        # substitute 2 parents have lower fitness than childrens
        child1_sub_flag = True
        child2_sub_flag = True
        for i in range(len(self._population)):
            # fitness evaluation
            self._evaluations += 1
            if ((self.collisions(self._population[i]) > self.collisions(child1)) and (child1_sub_flag is True)):
                self._population[i] = child1
                child1_sub_flag = False
            if ((self.collisions(self._population[i]) > self.collisions(child2)) and (child2_sub_flag is True)):
                self._population[i] = child2
                child2_sub_flag = False
                break


    # check if the termination conditions occurred
    def is_over(self):
        for permutation in self.population:
            if self.collisions(permutation) == 0:
                return [True, permutation]
        if(self.evaluations >= self.max_evaluations):
            return [True, 1]
        return [False]


    # Check if there is a queen in the diagonal
    def diagonal_queen(self, permutation, diagonal_place):
        for i in range(8):
            selected_queen = (i, permutation[i])
            if selected_queen == diagonal_place:
                return 1
        return 0


    # Check the number of queens that are killed
    # If there are not collisions, it's a solution
    def collisions(self, permutation):
        number_of_collisions = 0
        for i in range(8):
            queen_position = i
            diagonal_up = permutation[i]
            diagonal_down = permutation[i]
            for queen_position in range(queen_position + 1, 8):
                diagonal_up += 1
                diagonal_down -= 1
                if (diagonal_up < 8) and (self.diagonal_queen(permutation,
                   (queen_position, diagonal_up))):
                    number_of_collisions += 1
                if (diagonal_down > 0) and (self.diagonal_queen(permutation,
                   (queen_position, diagonal_down))):
                    number_of_collisions += 1
        return number_of_collisions


    # Recombine two parent and return two children
    def recombination(self, permutation1, permutation2):
        # For doing this example I will choose 2 parents random from the list population
        random1 = random.randint(0,self._population_size-1)
        random2 = random.randint(0,self._population_size-1)
        while random1 == random2:
            parent2 = random.randint(0,self._population_size-1)
        parent1 = self._population[random1]
        parent2 = self._population[random2]
        # Now, we create the array of the child
        child1 = np.zeros(8)
        child2 = np.zeros(8)
        # Triem el punt a partir del qual volem heretar
        random_point = random.randint(1, 7)
        for i in range(random_point):
            child1[i] = parent1[i]
            child2[i] = parent2[i]
        # Ara omplim la resta amb la info de laltre, agafant els valors no repetits.
        for i in range(random_point, 8):
            for j in range(8):
                if (not(parent2[(j + random_point) % 8] in child1)
                and (child1[i] == 0)):
                    child1[i] = parent2[(j + random_point) % 8]
                if (not(parent1[(j + random_point) % 8] in child2)
                and (child2[i] == 0)):
                    child2[i] = parent1[(j + random_point) % 8]
        return child1, child2


    # Mutate one permutation
    def mutation(self, permutation):
        if random.random() < 0.9:
            gen1, gen2 = random.sample(range(0,7),2)
            _permutation = copy.deepcopy(permutation)
            _permutation[gen1], _permutation[gen2] = _permutation[gen2], _permutation[gen1]
            return _permutation
        return permutation        


    # solve N-Queen problem
    def get_solution(self):
        n_generation = 0
        while not self.is_over()[0]:
            self.generation()
            n_generation += 1
            print "Generation {}".format(n_generation)
        if self.is_over()[0] == True and self.is_over() != 1:
            return self.is_over()[1], n_generation
        else:
            return "Feels bad", n_generation


    def print_solution(self):
        # Define de color map and array of possible locations of queens
        cmap = ListedColormap(['white', 'black'])
        queens = np.zeros((8,8))
        array, generation = self.get_solution()
        print type(array[0])
        if type(array) is not str:
            print "The solution is {}".format(array)
            j=0
            for i in array:
                queens[int(i)-1,j] = 1
                j+=1
            plt.matshow(queens, cmap=cmap, extent=[0, 8, 0, 8])
            plt.grid()
            print "Solved in generation {} with {} evalutations".format(generation, self.evaluations)
        else: print array

array = eight_queens_problem()
array.print_solution()