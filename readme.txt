Authors: Joan Marc Soler, Pol Rovira

In this repository there are two archives:

- main.py where you can execute the 8-queens solution and get
a black and white chess filled by the queens.

- operators.py where the genetic algorithm from main get some
operations.

Our goal is compact everything in one file.

IMPORTANT: Run the project with Python 2